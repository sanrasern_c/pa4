package readability;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;
/**
 * This class is to count words , syllables and sentences.
 * @author Sanrasern 5710547247
 */
public class WordCounter {
	/**Initialize State.*/
	State state;
	/**Initialize Attributes.*/
	private int count = 0;
	private int words = 0;
	private int sentences = 0;
	private boolean check = false;
	protected int totalSyllables = 0;
	private double index = 0;
	private String readability;

	/**
	 * To run program.
	 * @param text is text from source.
	 */
	public void run( InputStream text ) {
		try {
			BufferedReader breader = new BufferedReader(new InputStreamReader(text));
			while(breader.ready()) {
				String temp = "";
				while(true) {
					char c = (char) breader.read();
					if(Character.isWhitespace(c)) {
						setCheck();
						break;
					}
					temp += c;
					if("[.!?:]".indexOf(c) >= 0 && getCheck()) {
						temp = temp.substring(0, temp.length() - 1);
						sentences++;
						break;
					}
				}

				temp = temp.replaceAll("[/()\",;-]" , "");
				int count = countSyllables(temp);
				totalSyllables += count;
			}
			System.out.print(printAns());
			
		}catch (IOException e) {
			e.printStackTrace();
		}
	}



	/**
	 * To count syllables from word.
	 * @param word is word that going to find out.
	 * @return the number of syllables.
	 */
	public int countSyllables (String word) {
		count = 0;
		state = startState;
		for ( char c : word.toCharArray() ) {
			state.handleChar(c);
		}
		if(state == dashState) {
			count = 0;
		}
		else if( state == eState && count != 1) {
			count--;
		}

		if(count > 0) {
			words++;
			check = true;
		}

		return count;
	}

	/**
	 * To get the number of words.
	 * @return number of words.
	 */
	public int getWords() {
		return words;
	}

	/**
	 * To check that is it a vowel.
	 * @param c is char that going to check.
	 * @return true if c is  a character , false if is not.
	 */
	public boolean isVowel (char c) {
		String vowelList = "aeiouAEIOU";
		String temp = c+"";
		return vowelList.contains(temp);

	}
	/**To check the boolean.*/
	public void setCheck() {
		check = false;
	}
	
	/**To get the check.*/
	public boolean getCheck() {
		return check;
	}

	/**
	 * Count all words read from an input stream an return the total count.
	 * @param instream is the input stream to read from.
	 * @return number of words in the InputStream.  -1 if the stream
	 *        cannot be read.
	 */	
	public int countWords( URL url ) {
		int newCount =0 ;
		try{
			InputStream in = url.openStream();
			BufferedReader breader = new BufferedReader(new InputStreamReader(in ));
			while( true ) {
				String line = breader.readLine();
				if( line == null ) break;
				totalSyllables += countSyllables(line);
				newCount++;
			}
				
		}catch (IOException e) {
		}
		return newCount;
	}

	

	/**
	 * This is the first state that first character of the word is in. 
	 */
	State startState = new State() {
		public void handleChar ( char c ) {
			/**If is vowel e or E its go to eState.*/
			if(c=='e' || c=='E') {
				state = eState;
				count++;
			}
			/**If is y or Y its go to vowelState.*/
			else if ( isVowel(c) || c=='y' || c=='Y' ){
				state = vowelState;
				count++;
			}
			/**If is character its go to cosonantState.*/
			else if( Character.isLetter(c) )
				state = consonantState;
			else
				state = nonWordState;
		}
	};

	/**
	 * This is the state that when character is a consonant.
	 */
	State consonantState = new State() {
		public void handleChar (char c) {
			/**If is vowel e or E its go to eState.*/
			if( c=='e' || c=='E' ){
				count++;
				state = eState;
			}
			/**If is y , Y or vowel its go to vowelState.*/
			else if ( isVowel (c) || c=='y' || c=='Y' ){
				count++;
				state = vowelState;
			}
			/**If is character its go to cosonantState.*/
			else if (Character.isLetter(c) )
				state = consonantState;
			/**If is - its go to dashState.*/
			else if ( c == '-' ) 
				state = dashState;
			/**If is \ its go to dashState.*/
			else if (c=='\'')
				state = consonantState;
			/**None of above conditions its go to nonWordState.*/
			else
				state = nonWordState;
		}
	};

	/**
	 * This is the state when character is vowel.
	 */
	State vowelState = new State() {
		public void handleChar ( char c ) {
			 /**If is vowel its go to vowelState.*/
			if( isVowel(c)) 
				state = vowelState;
			/**If c isn't vowel and c is a consonant  its go to consonantState.*/
			else if( Character.isLetter(c) && !isVowel(c) ) 
				state = consonantState;
			/**If is - its go to dashState.*/
			else if ( c=='-' )
				state = dashState;
			/**None of above conditions its go to nonWordState.*/
			else 
				state = nonWordState;
		}
	};

	/**
	 * This is a state when character is e . 
	 */
	State eState = new State() {
		public void handleChar (char c) {
			 /**If is vowel its go to vowelState.*/
			if( isVowel(c) )
				state = vowelState;
			/**If is character its go to cosonantState.*/
			else if( Character.isLetter(c) )
				state = consonantState;
			/**If is - its go to dashState.*/
			else if( c=='-' )
				state = dashState;
			/**None of above conditions its go to nonWordState.*/
			else
				state = nonWordState;
		}
	};

	/**
	 * This is s state when found dash.
	 */
	State dashState = new State() {
		public void handleChar (char c) {
			/**If is vowel its go to vowelState.*/
			if( isVowel(c) ) {
				count++;
				state = vowelState;
			}
			/**If is character its go to cosonantState.*/
			else if( Character.isLetter(c) )
				state = consonantState;
			/**None of above conditions its go to nonWordState.*/
			else
				state = nonWordState;
		}
	};

	/**
	 * This is a state when we found that word is non-word.
	 */
	State nonWordState = new State() {
		public void handleChar (char c) {
			count = 0;
		}
	};
	
	/**
	 * To get format and result of he program.
	 * @return result of the program.
	 */
	public String printAns(){
		String temp1 = "Number of Syllables:    ";
		String temp2 = totalSyllables+"\n";
		String temp3 = "Number of Words:    ";
		String temp4 = getWords()+"\n";
		String temp5 = "Number of Sentences:    ";
		String temp6 = sentences+"\n";
		
		/**Formula to compute Readability index. */
		index = 206.835 - 84.6*(totalSyllables *1.0/ getWords() ) - 1.015*(getWords() *1.0/ sentences );
		
		/**To order a Readability.*/
		if( index > 100 ) readability = "4th grade student (elementary school)";
		else if ( index > 90 && index <= 100 ) readability = "5th grade student";
		else if ( index > 80 && index <= 90 ) readability = "6th grade student";
		else if ( index > 70 && index <= 80 ) readability = "7th grade student";
		else if ( index > 65 && index <= 70 ) readability = "8th grade student";
		else if ( index > 60 && index <= 65 ) readability = "9th grade student";
		else if ( index > 50 && index <= 60 ) readability = "High school student";
		else if ( index > 30 && index <= 50 ) readability = "College student";
		else if ( index >= 0 && index <= 30 ) readability = "College graduate";
		else readability = "Advanced degree graduate";

		
		String temp7 = "Flesch Index:    ";
		String temp8 = String.format("%.1f\n" , index);
		String temp9 = "Readability:    ";
		String temp10 = readability + "\n";
		
		/**A String of result.*/
		String result = temp1+temp2+temp3+temp4+temp5+temp6+temp7+temp8+temp9+temp10;
		return result;
		
		}

}


