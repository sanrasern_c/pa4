package readability;
/**
 * This is state interface.
 * @author Sanrasern 5710547247
 */
public interface State {
	public void handleChar (char c);
	
	
}
