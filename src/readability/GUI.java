package readability;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
/**
 * This is GUI of the program.
 * @author Sanrasern 5710547247
 *
 */
public class GUI extends JFrame {
	/**
	 * Create attributes.
	 */
	private JLabel fileOrUrl;
	private JTextField input;
	private JButton browse, count, clear;
	private JTextArea area;
	
	/**
	 * Create WordCounter.
	 */
	WordCounter wc  = new WordCounter();

	/**
	 * Constructor of the class.
	 */
	public GUI() {
		initComponents();
	}
	
	/**
	 * To initializing components of GUI.
	 */
	public void initComponents() {
		/**To set the title name*/
		super.setTitle( "File Counter" );
		
		/**Initialize panel*/
		JPanel pane = new JPanel();
		/**Set layout to be BoxLayout*/
		pane.setLayout( new BoxLayout( pane , BoxLayout.Y_AXIS ) );
		
		/**Set another layout to be flow layout*/
		JPanel flow1 = new JPanel();
		flow1.setLayout( new FlowLayout() );
		
		/**Initialize fileOrUrl label.*/
		fileOrUrl = new JLabel( "File or URL name: " );
		
		/**Initialize textField.*/
		input = new JTextField(15);

		/**Initialize browse button.*/
		browse = new JButton( "Browse..." );
		browse.addActionListener( new Browse() );

		/**Initialize count button.*/
		count = new JButton( "Count" );
		count.addActionListener( new Count() );

		/**Initialize clear button.*/
		clear = new JButton( "Clear" );
		clear.addActionListener( new Clear() );

		/**
		 * Add components to panel.
		 */
		flow1.add( fileOrUrl );
		flow1.add( input );
		flow1.add( browse );
		flow1.add( count );
		flow1.add( clear );

		/**Initialize panel and set to be flow layout. */
		JPanel flow2 = new JPanel();
		flow2.setLayout( new FlowLayout() );
		
		/**Initialize textArea of result.*/
		area = new JTextArea(7, 50);
		JScrollPane scroll = new JScrollPane(area);
		
		/**Add scorePane in panel.*/
		flow2.add( scroll );
		
		/**Add things in to panel.*/
		pane.add( flow1 );
		pane.add( flow2 );
		this.add(pane);
		this.pack();
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);	
	}
	
	/**
	 * This is a instant class of browse.
	 */
	class Browse implements ActionListener {
		public void actionPerformed( ActionEvent e ) {
			FileChooser fc = new FileChooser();
			input.setText(fc.getFileName());
		}
	}
	
	public static void main(String[] args) {
		GUI ui = new GUI();
	}
	
	/**This is instant class of count button.*/
	public class Count implements ActionListener {
		public void actionPerformed( ActionEvent e ) {
			/**Initialize WordCounter.*/
			WordCounter wc = new WordCounter();
			String name = input.getText();
			/**To check Is a URL.*/
			if(name.contains("http://")) {
				URL url = null;
				/**To run from URL.*/
				try {
					url = new URL(name);
				} catch (MalformedURLException e1) {
					e1.printStackTrace();
				}
				InputStream text = null;
				try {
					text = url.openStream();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				wc.run(text);
				area.setText(wc.printAns());
				area.setEditable(false);
			}
			/**To run from file.*/
			else {
				File file = new File(name);
				InputStream text = null;
				try {
					text = new FileInputStream(file);
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
				wc.run(text);
				area.setText(wc.printAns());
				area.setEditable(false);
			}
				
		}
	}
	
	/**This is instant class of clear button.*/
	public class Clear implements ActionListener {
		public void actionPerformed( ActionEvent e ) {
			input.setText("");
		}
	}

}
