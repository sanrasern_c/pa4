package readability.Readability;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import readability.GUI;
import readability.WordCounter;

/**
 * To run a program.
 * @author Sanrasern 5710547247
 */
public class Main {
	public static void main (String [] args) {
		int length = args.length;
		if(length==0) {
			GUI ui = new GUI();
		}
		else {
			String name = args[0];
			WordCounter wc = new WordCounter();
			if(name.contains("http://")) {
				URL url = null;
				try {
					url = new URL(name);
				}catch (MalformedURLException e1) {
					e1.printStackTrace();
				}
				InputStream text = null;
				try {
					text = url.openStream();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				wc.run(text);
				System.out.print(wc.printAns());
			}
			else {
				File file = new File(name);
				InputStream text = null;
				try {
					text = new FileInputStream(file);
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
				wc.run(text);
				System.out.print(wc.printAns());
			}
			
		}
	}
}
