package readability;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
/**
 * This is part of browse button.
 * @author Sanrasern 5710547247
 */
public class FileChooser {
	/** Get the file name. */
	public String getFileName() {
		JFileChooser fc = new JFileChooser();
		FileFilter filter = new TextFileFilter();
		fc.addChoosableFileFilter( filter );
		fc.setCurrentDirectory( new File("c:/") );

		int result = fc.showDialog(null, "Open");
		if( result == JFileChooser.APPROVE_OPTION )
			return fc.getSelectedFile().getAbsolutePath();
		else 
			return null;
	}
	
	/**To check that is a text file.*/
	class TextFileFilter extends FileFilter {
		public boolean accept (File f) {
			if( f.isDirectory() ) return true;
			if( ! f.isFile() ) return false;
			String ext = getExtension( f );
			if( ext.equals("txt")) return true;
			if( ext.equals("htm")) return true;
			if( ext.equals("html")) return true;
			return false;
		}
		
		/**
		 *To get a txt files to choose.
		 */
		public String getDescription() {
			return "Text (.txt) Files";
		}
		
		/**
		 * To show what the kind of file.
		 * @param f is that file to show the file
		 * @return kind name of the file.
		 */
		String getExtension( File f ) {
			String filename = f.getName();
			int k = filename.lastIndexOf('.');
			if( k<=0 || k>= filename.length()-1) return "";
			else
				return filename.substring(k+1).toLowerCase();
		}
	}


}
